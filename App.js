import React, { Component } from 'react';

import { MainRouter } from 'app/src/MainRouter.js';

export default class App extends Component {
  render() {
    return (
      <MainRouter/>
    );
  }
}
