import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
import {
  Text,
  TouchableOpacity
} from 'react-native';

import SplashScreen from 'app/src/pages/SplashScreen.js';
import Home from 'app/src/pages/Home.js';
import Detail from 'app/src/pages/Detail.js';
import Movie from 'app/src/pages/Movie.js';
import Login from 'app/src/pages/Login.js';
import Mpin from 'app/src/pages/Mpin.js';
import Transaksi from 'app/src/pages/Transaksi.js';
import Main from 'app/src/pages/Main.js';

export const MainRouter = createStackNavigator({
  SplashScreenPage: {
    screen: SplashScreen
  },
  LoginPage: {
    screen: Login
  },
  HomePage: {
    screen: Home
  },
  DetailPage: {
    screen: Detail
  },
  MoviePage: {
    screen: Movie
  },
  MpinPage: {
    screen: Mpin
  },
  TransaksiPage: {
    screen: Transaksi
  },
  MainPage: {
    screen: Main,
    navigationOptions: ({ navigation }) => ({
      title: 'AppName',
      headerLeft: <TouchableOpacity onPress={ () => navigation.openDrawer() }><Text style={{color: 'white'}}>Menu</Text></TouchableOpacity>,
      headerRight: <TouchableOpacity onPress={ () => navigation.openDrawer() }><Text style={{color: 'white'}}>Menu</Text></TouchableOpacity>,
      headerStyle: {
        backgroundColor: 'red',
        height: 70,
      },
      headerTintColor: 'white',
      headerTitleStyle: { fontSize: 16 },
    })
  },
},
{
  initialRouteName: 'SplashScreenPage',
});
