import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  View
} from 'react-native';

export default class HotelItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress} style={styles.container}>
        <View style={styles.image_wrapper}>
          <Image style={styles.image} source={this.props.image}/>
          <Text style={styles.discount}>{this.props.discount}</Text>
        </View>
        <View style={styles.content}>
          <Text style={styles.title}>{this.props.title}</Text>
          <View style={styles.info_content}>
            <Text style={styles.price}>{this.props.price}</Text>
            <Text style={styles.night}>{this.props.night}</Text>
          </View>
          <Text style={styles.description}>{this.props.description}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.12)',
  },
  content: {
    padding: 25,
  },
  image: {
    resizeMode: 'cover',
    width: '100%',
  },
  discount: {
    backgroundColor: '#3cc34e',
    borderRadius: 2,
    bottom: 25,
    color: '#8fff9c',
    fontSize: 14,
    fontWeight: 'bold',
    right: 25,
    paddingHorizontal: 6,
    paddingVertical: 7,
    position: 'absolute',
  },
  title: {
    color: '#333333',
    fontSize: 16,
    fontWeight: 'bold',
  },
  info_content: {
    flexDirection: 'row',
    marginTop: 16,
  },
  price: {
    color: '#333',
    fontSize: 14,
    fontWeight: 'bold',
    marginRight: 10,
  },
  night: {
    color: '#333',
    fontSize: 14,
    fontWeight: 'bold',
  },
  description: {
    color: '#585e66',
    fontSize: 14,
    lineHeight: 21,
    marginTop: 16,
  }
});
