import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Image
} from 'react-native';

export default class OprItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress} style={styles.container}>
        <Image
          style={{width: 50, height: 50, alignSelf: 'center'}}
          source={{uri: (this.props.image) ? this.props.image : 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
        />
        <Text style={styles.text}>{this.props.operator}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: '#ccc',
    padding: 20,
    width: '50%',
    minHeight: 50,
    justifyContent: 'center',
  },
  text: {
    textAlign: 'center',
  }
});
