import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  ScrollView,
} from 'react-native';

export default class Detail extends Component {
  render() {
    const idParam = this.props.navigation.getParam("id");

    return (
      <ScrollView style={styles.container}>
        <Text onPress={() => this.props.navigation.goBack()}>Kembali</Text>
        <Text>Detail ID: {idParam}</Text>
      </ScrollView>
    );
  }
}

Detail.navigationOptions = {
  title: "Detail"
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
});
