import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  ScrollView,
  View,
  Image,
  Dimensions,
} from 'react-native';

import Swiper from 'react-native-swiper';

export default class Home extends Component {
  render() {
    const imageHeight = (250 / 512) * Dimensions.get('window').width;
    console.warn(Dimensions.get('window').width + ' x ' + imageHeight);
    
    return (
      <ScrollView style={{flexGrow: 1}}>
        <View style={{height: imageHeight}}>
          <Swiper
            style={{flex: 1}}
            showsButtons={true}
            autoplay={true}
            loop={false}
            dotColor={'white'}
            activeDotColor={'#ccc'}
            nextButton={<Text style={{color: 'white', fontSize: 36}}>›</Text>}
            prevButton={<Text style={{color: 'white', fontSize: 36}}>‹</Text>}
          >
            <View style={{flex: 1}}>
              <Image style={{width: '100%', height: imageHeight}} source={{uri: 'https://lh3.googleusercontent.com/-0DoA1LgQ2teMP215TJ0rDDbf1vZbjlm8yueCPXGj0yhdq7FBeqYiI-6oqW6VA72yw'}}/>
            </View>
            <View style={{flex: 1}}>
              <Image style={{width: '100%', height: imageHeight}} source={{uri: 'https://lh3.googleusercontent.com/-0DoA1LgQ2teMP215TJ0rDDbf1vZbjlm8yueCPXGj0yhdq7FBeqYiI-6oqW6VA72yw'}}/>
            </View>
            <View style={{flex: 1}}>
              <Image style={{width: '100%', height: imageHeight}} source={{uri: 'https://lh3.googleusercontent.com/-0DoA1LgQ2teMP215TJ0rDDbf1vZbjlm8yueCPXGj0yhdq7FBeqYiI-6oqW6VA72yw'}}/>
            </View>
          </Swiper>
        </View>
      </ScrollView>
    );
  }
}

Home.navigationOptions = {
  header: null,
  drawerLabel: 'Beranda',
}

const styles = StyleSheet.create({
});
