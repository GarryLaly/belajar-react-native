import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  ActivityIndicator,
  AsyncStorage,
  Clipboard
} from "react-native";
import axios from "axios";
import DeviceInfo from "react-native-device-info";
import { StackActions, NavigationActions } from "react-navigation";
import moment from "moment";
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";
import FCM, {
  FCMEvent,
  WillPresentNotificationResult,
  NotificationType
} from "react-native-fcm";

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ApiType: "Login",
      DeviceId: DeviceInfo.getUniqueID(),
      // DeviceId: '9f6eeac460071e10',
      UserName: "+6281232009362",
      Password: "485694",
      DateTime: moment().format("YYYYMMDDHHmmss"),
      loading: false,
      currentLatitude: 37.78825,
      currentLongitude: -122.4324
    };
  }

  async componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          currentLatitude: position.coords.latitude,
          currentLongitude: position.coords.longitude
        });
        console.warn(JSON.stringify(position));
        this.map.animateToRegion({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.0122,
          longitudeDelta: 0.0021
        });
      },
      error => console.error(error),
      { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
    );

    const token = await AsyncStorage.getItem("token");
    if (token) {
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "MainPage" })]
      });
      this.props.navigation.dispatch(resetAction);
    }

    FCM.on(FCMEvent.Notification, notif => {
      console.warn("Notification", notif);
      if (notif.targetScreen === "MPin") {
        this.props.navigation.navigate("MpinPage");
      }
    });

    FCM.on(FCMEvent.RefreshToken, token => {
      console.warn("TOKEN (refreshUnsubscribe)", token);
    });
    FCM.getInitialNotification().then(notif => {
      console.warn("Notification Init", notif);
    });

    try {
      let result = await FCM.requestPermissions({
        badge: false,
        sound: true,
        alert: true
      });
    } catch (e) {
      console.error(e);
    }

    FCM.getFCMToken().then(token => {
      console.warn("TOKEN (getFCMToken)", token);
      Clipboard.setString(token);
    });

    // topic example
    FCM.subscribeToTopic("global");
    // https://fcm.googleapis.com/fcm/send
    // Header: {
    // Authorization: key=[Legacy server key]
    // }
    // {
    //   "to": "/topics/global",
    //   "data": {
    //     "custom_notification": {
    //           "body": "Test",
    //           "title": "Judul Test",
    //           "priority": "high",
    //           "sound": "default",
    //           "show_in_foreground": true
    //         },
    //         "type": "notification",
    //         "targetScreen": "MPin"
    //   }
    // }
    // FCM.unsubscribeFromTopic('sometopic')
  }

  async submitLogin() {
    this.setState({ loading: true });
    try {
      const userLogin = await axios.post(
        "http://192.168.1.211:9000",
        this.state
      );

      // Jika berhasil Login
      if (userLogin.data.ResponseCode == "000") {
        await AsyncStorage.setItem("token", userLogin.data.Token);
        await AsyncStorage.setItem("username", this.state.UserName);
        await AsyncStorage.setItem("password", this.state.Password);
        console.warn(JSON.stringify(userLogin));
        this.props.navigation.navigate("MpinPage");
      }
      this.setState({ loading: false });

      alert(userLogin.data.Messages);
    } catch (error) {
      console.error(error);
      alert(error);
      this.setState({ loading: false });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Login</Text>
        <MapView
          provider={PROVIDER_GOOGLE}
          ref={map => {
            this.map = map;
          }}
          style={{ width: 300, height: 300 }}
          showsUserLocation={true}
          showsMyLocationButton={true}
          showsCompass={true}
          followsUserLocation={true}
          loadingEnabled={true}
          toolbarEnabled={true}
          zoomEnabled={true}
          rotateEnabled={true}
          initialRegion={{
            latitude: this.state.currentLatitude,
            longitude: this.state.currentLongitude,
            latitudeDelta: 0.0122,
            longitudeDelta: 0.0021
          }}
          onRegionChangeComplete={region => {
            this.setState({
              currentLatitude: region.latitude,
              currentLongitude: region.longitude
            });
            console.warn(JSON.stringify(region));
            this.map.animateToRegion({
              latitude: region.latitude,
              longitude: region.longitude,
              latitudeDelta: 0.0122,
              longitudeDelta: 0.0021
            });
          }}
        >
          <Marker
            coordinate={{
              latitude: this.state.currentLatitude,
              longitude: this.state.currentLongitude
            }}
            title="Lokasi Saya"
            description="Lokasi"
          />
        </MapView>
        <TextInput
          style={{ width: "100%" }}
          placeholder="Phone"
          value={this.state.UserName}
          onChangeText={text => this.setState({ UserName: text })}
        />
        <TextInput
          style={{ width: "100%" }}
          placeholder="Password"
          secureTextEntry={true}
          value={this.state.Password}
          onChangeText={text => this.setState({ Password: text })}
        />
        {!this.state.loading && (
          <Button title="Login" onPress={() => this.submitLogin()} />
        )}
        {this.state.loading && (
          <ActivityIndicator size="small" color="#0000ff" />
        )}
      </View>
    );
  }
}

Login.navigationOptions = {
  header: null
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F5FCFF"
  },
  title: {
    fontSize: 24,
    textAlign: "center",
    marginBottom: 20
  }
});
