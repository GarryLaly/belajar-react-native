import { createDrawerNavigator } from 'react-navigation';

import Home from 'app/src/pages/Home.js';
import Operator from 'app/src/pages/Operator.js';
import Screen1 from 'app/src/pages/Screen1.js';
import Screen2 from 'app/src/pages/Screen2.js';

export default createDrawerNavigator({
  Home: {
    screen: Home,
  },
  Operator: {
    screen: Operator,
  },
  About: {
    screen: Screen2,
  },
});
