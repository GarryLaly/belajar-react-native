import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  ScrollView,
} from 'react-native';
import axios from 'axios';

import MovieItem from 'app/src/components/MovieItem.js';

export default class Movie extends Component {
  constructor(props) {
    super(props);

    this.state = {
      movies: [],
    }
  }

  async getMovies() {
    const movies = await axios.get('https://facebook.github.io/react-native/movies.json?secret=423424adadasd');
    this.setState({
      movies: movies.data.movies
    })
  }

  componentDidMount() {
    this.getMovies();
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        {this.state.movies.map(function(movieItem) {
          return (
            <MovieItem
              id={movieItem.id}
              title={movieItem.title}
            />
          )
        })}

      </ScrollView>
    );
  }
}

Movie.navigationOptions = {
  header: null
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
});
