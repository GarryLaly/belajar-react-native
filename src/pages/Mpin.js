import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  AsyncStorage
} from 'react-native';
import axios from 'axios';
import DeviceInfo from 'react-native-device-info';
import moment from 'moment';

export default class Mpin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ApiType: 'Confirm',
      DeviceId: DeviceInfo.getUniqueID(),
      // DeviceId: '9f6eeac460071e10',
      UserName: '',
      Password: '',
      MPin: '',
      DateTime: moment().format("YYYYMMDDHHmmss"),
    }
  }

  async componentDidMount() {
    const UserName = await AsyncStorage.getItem("username");
    const Password = await AsyncStorage.getItem("password");
    this.setState({
      UserName: UserName,
      Password: Password,
    });
  }

  async submitMpin() {
    const userMpin = await axios.post('http://192.168.1.211:9000', this.state);

    // Jika berhasil Mpin
    if (userMpin.data.ResponseCode == '000') {
      await AsyncStorage.setItem("balance", userMpin.data.Balance);
      console.warn(JSON.stringify(userMpin));
      this.props.navigation.navigate("MainPage");
    }

    alert(userMpin.data.Messages);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Mpin</Text>
        <TextInput
          style={{width: '100%'}}
          placeholder="Mpin"
          secureTextEntry={true}
          value={this.state.MPin}
          onChangeText={(text) => this.setState({MPin: text})}
        />
        <Button
          title="Masukkan Mpin"
          onPress={() => this.submitMpin()}
        />
      </View>
    );
  }
}

Mpin.navigationOptions = {
  header: null
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  title: {
    fontSize: 24,
    textAlign: 'center',
    marginBottom: 20,
  }
});
