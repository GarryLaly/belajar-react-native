import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  ScrollView,
  AsyncStorage,
  TouchableOpacity,
  View,
} from 'react-native';
import axios from 'axios';
import DeviceInfo from 'react-native-device-info';
import { StackActions, NavigationActions } from 'react-navigation';
import jsSHA512 from 'js-sha512';
import moment from 'moment';

import OprItem from 'app/src/components/OprItem.js';

export default class Operator extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ApiType: 'GetOpr',
      DeviceId: DeviceInfo.getUniqueID(),
      // DeviceId: '9f6eeac460071e10',
      UserName: '',
      Password: '',
      DateTime: moment().format("YYYYMMDDHHmmss"),
      Signature: '',
      operator: []
    }
  }

  async componentDidMount() {
    const UserName = await AsyncStorage.getItem("username");
    const Password = await AsyncStorage.getItem("password");
    this.setState({
      UserName: UserName,
      Password: Password,
    });

    const token = await AsyncStorage.getItem("token");
    const hmac = jsSHA512.hmac(this.state.DeviceId, this.state.UserName + this.state.DateTime + this.state.ApiType + token);

    this.setState({Signature: hmac});

    const getOperator = await axios.post('http://192.168.1.211:9000', this.state);

    if (getOperator.data.ResponseCode == '000') {
      this.setState({operator: getOperator.data.Data});
    }else {
      alert(getOperator.data.Messages);
      this.logoutUser();
    }

    console.warn(JSON.stringify(getOperator.data));
  }

  async logoutUser() {
    await AsyncStorage.setItem("token", "");
    await AsyncStorage.setItem("username", "");
    await AsyncStorage.setItem("password", "");
    await AsyncStorage.setItem("balance", "");

    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'LoginPage' })],
    });
    this.props.navigation.dispatch(resetAction);
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <ScrollView style={styles.container}>
        <TouchableOpacity onPress={() => this.logoutUser()}>
          <Text>Keluar Akun</Text>
        </TouchableOpacity>
        <View style={styles.kolom_menu}>
          {this.state.operator.map(function(value) {
            if (value.kel == 'VOUCHER') {
              return (
                <OprItem
                  image={value.url}
                  operator={value.opr}
                  onPress={() => navigate('TransaksiPage', {opr: value.opr})}
                />
              )
            }
          })}
        </View>
      </ScrollView>
    );
  }
}

Operator.navigationOptions = {
  header: null
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  kolom_menu: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
});
