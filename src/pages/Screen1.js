import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
} from 'react-native';

export default class Screen1 extends Component {
  render() {
    return (
      <View>
        <Text>Screen1</Text>
        <Button title="Open Drawer" onPress={() => this.props.navigation.openDrawer()}/>
      </View>
    );
  }
}

Screen1.navigationOptions = {
  drawerLabel: 'Beranda',
}

const styles = StyleSheet.create({
});
