import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';

export default class Screen2 extends Component {
  render() {
    return (
      <View>
        <Text>Screen2</Text>
      </View>
    );
  }
}

Screen2.navigationOptions = {
  header: null,
  drawerLabel: 'Tentang',
}

const styles = StyleSheet.create({
});
