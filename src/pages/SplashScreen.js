import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';

export default class SplashScreen extends Component {

    componentDidMount() {
        const that = this;
        setTimeout(function(){
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'LoginPage' })],
            });
            that.props.navigation.dispatch(resetAction);
        }, 3000);
    }

  render() {
    return (
      <View>
        <Text>SplashScreen</Text>
      </View>
    );
  }
}

SplashScreen.navigationOptions = {
  header: null
}

const styles = StyleSheet.create({
});
