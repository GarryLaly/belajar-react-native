import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  ScrollView,
  AsyncStorage,
  TouchableOpacity,
  View,
  TextInput,
  ActivityIndicator,
  ToastAndroid,
} from 'react-native';
import { BASE_URL, COLOR } from 'app/src/config/variable.js';
import axios from 'axios';
import DeviceInfo from 'react-native-device-info';
import { StackActions, NavigationActions } from 'react-navigation';
import jsSHA512 from 'js-sha512';
import moment from 'moment';
import ContactsWrapper from 'react-native-contacts-wrapper';

import OprItem from 'app/src/components/OprItem.js';

export default class Transaksi extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ApiType: 'GetVoc',
      DeviceId: DeviceInfo.getUniqueID(),
      // DeviceId: '9f6eeac460071e10',
      UserName: '',
      Password: '',
      DateTime: moment().format("YYYYMMDDHHmmss"),
      Signature: '',
      Opr: this.props.navigation.state.params.opr,
      priceList: [],
      Msisdn: '',
    }
  }

  async componentDidMount() {
    const UserName = await AsyncStorage.getItem("username");
    const Password = await AsyncStorage.getItem("password");
    this.setState({
      UserName: UserName,
      Password: Password,
    });

    const token = await AsyncStorage.getItem("token");
    const hmac = jsSHA512.hmac(this.state.DeviceId, this.state.UserName + this.state.DateTime + this.state.ApiType + token);
    this.setState({Signature: hmac});

    const getVoucher = await axios.post(BASE_URL, this.state);

    if (getVoucher.data.ResponseCode == '000') {
      this.setState({priceList: getVoucher.data.Data});
    }else {
      alert(getVoucher.data.Messages);
    }

    console.warn(JSON.stringify(getVoucher.data));
  }

  getContactList() {
    ContactsWrapper.getEmail()
  .then((email) => {
    this.importingContactInfo = false;
    console.warn("email is", email);
    })
    .catch((error) => {
      this.importingContactInfo = false;
      console.warn("ERROR CODE: ", error.code);
      console.warn("ERROR MESSAGE: ", error.message);
      });
  }

  async beliPulsa(vtype) {
    if (this.state.Msisdn === "") {
      ToastAndroid.show("No. HP wajib di isi", ToastAndroid.SHORT);
      return;
    }

    this.setState({
      ApiType: 'TOPUP',
      Product: vtype,
    });
    const token = await AsyncStorage.getItem("token");
    const hmac = jsSHA512.hmac(this.state.DeviceId, this.state.UserName + this.state.DateTime + this.state.ApiType + token);
    this.setState({Signature: hmac});

    const postBeliPulsa = await axios.post(BASE_URL, this.state);

    if (postBeliPulsa.data.ResponseCode == '000') {
      // this.setState({priceList: postBeliPulsa.data.Data});
    }else {
      ToastAndroid.show(postBeliPulsa.data.Messages, ToastAndroid.SHORT);
    }

    console.warn(JSON.stringify(postBeliPulsa.data));
  }

  render() {
    const that = this;

    return (
      <View style={{flex: 1}}>
        <ScrollView style={styles.container}>
          <TextInput
            style={{width: '100%'}}
            placeholder="Phone"
            keyboardType="numeric"
            value={this.state.Msisdn}
            onChangeText={(text) => this.setState({Msisdn: text})}
          />
          <TouchableOpacity onPress={() => this.getContactList()}>
            <Text>Kontak</Text>
          </TouchableOpacity>
          {this.state.priceList.map(function(value, index) {
            return (
              <TouchableOpacity
                onPress={() => that.beliPulsa(value.vtype)}
                key={index}
                style={styles.priceItem}
              >
                <Text>{value.ket}</Text>
                <Text>{value.harga1}</Text>
              </TouchableOpacity>
            )
          })}
        </ScrollView>
        <View style={{position: 'absolute', width: '100%', height: '100%', backgroundColor: 'rgba(0,0,0,0.1)', flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator sizer="large" color="#fff"/>
        </View>
      </View>
    );
  }
}

Transaksi.navigationOptions = {
  header: null
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  priceItem: {
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    padding: 10,
  },
});
